/**
 * @file	header.c
 * @date 	2016. 11. 25.
 * @author	D.M.SHIN
 * @brief	VCF header
 *
 * @history	2017.12.18 - define createVCFHeader() function
 * 			2018.02.28 - define getVCFHeaderSize() function
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "header.h"
#include "sample.h"

#define VCF_HEADER_COL_NUM 9

static const char* VCF_HEADER_COLS[VCF_HEADER_COL_NUM] = { "#CHROM", "POS",
		"ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT" };

static const char* DELIMITER_TAB = "\t";
static const char* DELIMITER_NEWLINE = "\n";

VCFHeader* newVCFHeader() {
	VCFHeader* header = malloc(sizeof(VCFHeader));

	header->header = NULL;
	header->sampleFilePath = NULL;

	header->init = initVCFHeader;
	header->load = loadVCFHeaderInfo;
	header->getColumnCount = getVCFHeaderColumnCount;
	header->getSize = getVCFHeaderSize;
	header->getText = getVCFHeaderText;
	header->dispose = disposeVCFHeader;

	return header;
}

void initVCFHeader(VCFHeader* this) {
	this->load(this);
}

void loadVCFHeaderInfo(VCFHeader* this) {
	this->header = newList();
	SList* headers = this->header;

	int i;
	for (i = 0; i < VCF_HEADER_COL_NUM; i++) {
		LItem* item = newLItem();
//		buf = strdup(VCF_HEADER_COLS[i]);
		item->set(item, strdup(VCF_HEADER_COLS[i]));
		headers->addData(headers, item);
	}

	// read header from sample file.
	Sample* sample = newSample();
	if (sample->open(sample, vcfOptions.sampleFilePath) == -1) {
		fprintf(stderr, "\n[ERROR] %s: %s (%s, line %d) \n", strerror(errno),
				__func__,
				__FILE__, __LINE__);
		exit(-1);
	}
	char readSampleIdBuf[256] = { 0, };
	/* skip the two header lines */
	sample->readLine(sample, readSampleIdBuf, sizeof(readSampleIdBuf) - 1);
	sample->readLine(sample, readSampleIdBuf, sizeof(readSampleIdBuf) - 1);

	char* token;
	while (sample->readLine(sample, readSampleIdBuf,
			sizeof(readSampleIdBuf) - 1) >= 0) {
		token = strtok(readSampleIdBuf, " ,\t");
		LItem* item = newLItem();
		//	buf = strdup(token);
		item->set(item, strdup(token));
		headers->addData(headers, item);
	}

	sample->dispose(sample);
}

size_t getVCFHeaderSize(VCFHeader* this) {
	SList* header = this->header;
	LItem* item;

	size_t size = 0;

	if ((item = header->getFirstData(header)) != NULL) {
		size += strlen(item->get(item));
	}

	while ((item = header->getNextData(header, item)) != NULL) {
		size += 1;
		size += strlen(item->get(item));
	}

	return size;
}

int getVCFHeaderColumnCount(VCFHeader* this) {
	SList* header = this->header;
	LItem* item;

	int cnt = 0;

	if ((item = header->getFirstData(header)) != NULL) {
		cnt++;
	}

	while ((item = header->getNextData(header, item)) != NULL) {
		cnt++;
	}

	return cnt;
}

int getVCFHeaderText(VCFHeader* this, char* text) {
	SList* header = this->header;
	LItem* item;

	if ((item = header->getFirstData(header)) != NULL) {
		strcpy(text, item->get(item));
	}

	while ((item = header->getNextData(header, item)) != NULL) {
		strcat(text, DELIMITER_TAB);
		strcat(text, item->get(item));
	}

	strcat(text, DELIMITER_NEWLINE);

	return strlen(text);
}

void disposeVCFHeader(VCFHeader* this) {
	SList* header = this->header;
	header->dispose(header);
}
