/**
 * @file	vcf.h
 * @date 	2018. 11. 5.
 * @author	D.M.SHIN
 * @brief	vcf
 *
 * @history	2018.12.4. - Declare the createtabix function
 *
 */

#ifndef VCFWRITER_H_
#define VCFWRITER_H_

#include <stdio.h>

#include "meta.h"
#include "header.h"
#include "bgzf.h"

BGZF* vcffp;

//int genDataFieldNum;

VCFMeta* vcfMeta;

VCFHeader* vcfHeader;

void initConvert();

void startConvert();

void destroyConvert();

int openVCFFile(const VCFOptions*);

size_t writeVCFMeta(VCFMeta*);

size_t writeVCFHeader(VCFHeader*);

void writeVCFData();

void convertToVCFFormat(char**, int);

void createtabix(const char*);

#endif /* VCFWRITER_H_ */
