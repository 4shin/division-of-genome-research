/**
 * @file	numberutils.h
 * @date 	2017. 8. 26.
 * @author	D.M.SHIN
 * @brief	numberutils
 */

#ifndef __NUMBERUTILS_H__
#define __NUMBERUTILS_H__

void removeFloatZeros(char*);

void doubleToString(double, char*, int);

#endif
