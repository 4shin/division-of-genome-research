/**
 * @file	header.h
 * @date 	2016. 11. 25.
 * @author	D.M.SHIN
 * @brief	VCF header
 *
 * @history	2017.12.18 - declare createVCFHeader() function
 * 			2018.02.28 - declare getVCFHeaderSize() function
 *
 */

#ifndef __HEADER_h__
#define __HEADER_h__

#include <stdio.h>

#include "list.h"
#include "vcfoption.h"

typedef struct _SVCFHeader {
	SList* header;
	const char* sampleFilePath;

	void (*init)(void*);
	void (*load)(void*);
	int (*getColumnCount)(void*);
	size_t (*getSize)(void*);
	int (*getText)(void*, char*);
	void (*dispose)(void*);
} VCFHeader;

VCFHeader* newVCFHeader();

void initVCFHeader(VCFHeader*);

void loadVCFHeaderInfo(VCFHeader*);

int getVCFHeaderColumnCount(VCFHeader*);

size_t getVCFHeaderSize(VCFHeader*);

int getVCFHeaderText(VCFHeader*, char*);

void disposeVCFHeader(VCFHeader*);

#endif
