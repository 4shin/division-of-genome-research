/**
 * @file	meta.h

 * @date 	2016. 11. 22.
 * @author	D.M.SHIN
 * @brief	meta information
 */

#ifndef __META_H__
#define __META_H__

#include "list.h"
#include "vcfoption.h"

typedef struct _SVCFMetaInfo {
	char* id;
	char* number;
	char* type;
	char* description;
	char* source;
	char* version;

	void (*setId)(void*, char*);
	void (*setNumber)(void*, char*);
	void (*setType)(void*, char*);
	void (*setDescription)(void*, char*);
	void (*setSource)(void*, char*);
	void (*setVersion)(void*, char*);
	void (*getString)(void*, char*);
	void (*dispose)(void*);
} VCFMetaInfo;

VCFMetaInfo* newVCFMetaInfo();

void setVCFMetaInfoId(VCFMetaInfo*, char*);

void setVCFMetaInfoNumber(VCFMetaInfo*, char*);

void setVCFMetaInfoType(VCFMetaInfo*, char*);

void setVCFMetaInfoDescription(VCFMetaInfo*, char*);

void setVCFMetaInfoSource(VCFMetaInfo*, char*);

void setVCFMetaInfoVersion(VCFMetaInfo*, char*);

void getVCFMetaInfoString(VCFMetaInfo*, char*);

void disposeVCFMetaInfo(VCFMetaInfo*);

typedef struct _SVCFMetaFilter {
	char* id;
	char* description;

	void (*setId)(void*, char*);
	void (*setDescription)(void*, char*);
	void (*dispose)(void*);
} VCFMetaFilter;

void newVCFMetaFilter();
void setVCFMetaFilterId(VCFMetaFilter*, char*);
void setVCFMetaFilterDescription(VCFMetaFilter*, char*);
void disposeVCFMetaFilter(VCFMetaFilter*);

typedef struct _SVCFMetaFormat {
	char* id;
	char* number;
	char* type;
	char* description;

	void (*setId)(void*, char*);
	void (*setNumber)(void*, char*);
	void (*setType)(void*, char*);
	void (*setDescription)(void*, char*);
	void (*getString)(void*, char*);
	void (*dispose)(void*);
} VCFMetaFormat;

VCFMetaFormat* newVCFMetaFormat();

/*
 * @brief	Set ID.
 * @param
 * */
void setVCFMetaFormatId(VCFMetaFormat*, char*);

/*
 * @brief	Set number in format.
 * @param
 * */
void setVCFMetaFormatNumber(VCFMetaFormat*, char*);

/*
 * @brief	Set type in format.
 * @param
 * */
void setVCFMetaFormatType(VCFMetaFormat*, char*);

/*
 * @brief	Set description in format.
 * @param
 * */

void setVCFMetaFormatDescription(VCFMetaFormat*, char*);
/*
 * @brief	Get format information to string.
 * @param
 * */
void getVCFMetaFormatString(VCFMetaFormat*, char*);

void disposeVCFMetaFormat(VCFMetaFormat*);

#define VCF_META_CONTIG_SIZE 5
#define VCF_META_INFO_SIZE 5
#define VCF_META_FORMAT_SIZE 5
#define VCF_META_FILTER_SIZE 5

#define VCF_VERSION "VCFv4.1"

typedef struct _SVCFMetaContig {
	char* id;
	long length;
	char* assembly;

	void (*setId)(void*, char*);
	void (*setLength)(void*, long);
	void (*setAssembly)(void*, char*);
	void (*dispose)(void*);
} VCFMetaContig;

void newVCFMetaContig();
void setVCFMetaContigId(VCFMetaContig*, char*);
void setVCFMetaContigLength(VCFMetaContig*, long);
void setVCFMetaContigAssembly(VCFMetaContig*, char*);
void disposeVCFMetaContig(VCFMetaContig*);

typedef struct _SVCFMeta {
	char* fileformat;
	char* fileDate;
	char* source;
	SList* contigList;
	char* reference;
	char* phasing;
	SList* infoList;
	SList* formatList;
	SList* filterList;

	void (*init)(void*);
	void (*setFileFormat)(void*, const char*);
	void (*setFileDate)(void*, const char*);
	void (*setSource)(void*, char*);
	void (*setContig)(void*, void*);
	void (*setReference)(void*, char*);
	void (*setPhasing)(void*, char*);
	void (*setInfo)(void*, void*);
	void (*setFormat)(void*, void*);
	void (*setFilter)(void*, void*);
	void* (*getFormat)(void*, int);
	int (*getText)(void*, char*);
	void (*dispose)(void*);
} VCFMeta;

VCFMeta* newVCFMeta();

void initVCFMeta(VCFMeta* meta);

void setVCFFileFormat(VCFMeta*, const char*);
void setVCFFileDate(VCFMeta*, const char*);
void setVCFSource(VCFMeta*, char*);
void setVCFContig(VCFMeta*, SList*);
void setVCFReference(VCFMeta*, char*);
void setVCFPhasing(VCFMeta*, char*);
void setVCFInfo(VCFMeta*, SList*);
void setVCFFormat(VCFMeta*, SList*);
void setVCFFilter(VCFMeta*, SList*);

int getVCFMetaText(VCFMeta*, char*);
void disposeVCFMeta(VCFMeta*);

#endif
