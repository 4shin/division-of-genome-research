/**
 * @file	vcfoption.h
 * @date 	2017. 12. 12.
 * @author	D.M.SHIN
 * @brief	Tool Option
 *
 * @history	2018.02.27. - Add two variables, 'r2_yn' and 'is_yn'
 *          2019.04.26. - Add a new variable 'gzipYN'
 */

#ifndef VCFOPTION_H_
#define VCFOPTION_H_

#include <stdio.h>

typedef enum { false, true } boolean;

typedef struct _SVCFOptions {
	char* genFilePath;
	char* infoFilePath;
	char* sampleFilePath;
	char* outputPath;
	char* chr;
	unsigned short int digits;
	boolean r2Yn;
	boolean isYn;
	boolean progressYn;
	boolean gzipYN;
} VCFOptions;

VCFOptions vcfOptions;

void initVCFOptions();

void setGenFilePath(char*);
char* getGenFilePath();

void setInfoFilePath(char*);
char* getInfoFilePath();

void setSampleFilePath(char*);
char* getSampleFilePath();

void setOutputPath(char*);
char* getOutputPath();

void setChrom(char*);
char* getChrom();

void setDigits(int);
int getDigits();

void setRsquaredEnable(boolean);
boolean getRsquaredEnable();

void setInfoScoreEnable(boolean);
boolean getInfoScoreEnable();

void setProgressEnable(boolean);
boolean getProgressEnable();

void setGZIPEnable(boolean);
boolean getGZIPEnable();

void setThreads(int);
int getThreads();

#endif /* VCFOPTION_H_ */
