/**
 * @file	sample.c
 * @date 	2016. 11. 11.
 * @author	D.M.SHIN
 * @brief	sample
 */

#include "sample.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

Sample* newSample() {
	Sample* sample = (Sample*) malloc(sizeof(Sample));

	sample->open = openSampleFile;
	sample->readLine = readLineSampleData;
	sample->getCount = getSampleCount;
	sample->dispose = disposeSample;

	return sample;
}

int openSampleFile(Sample* this, const char* filePath) {
	FILE* fp;

	if ((fp = fopen(filePath, "r")) == NULL) {
		fprintf(stderr, "\n[ERROR] %s: %s (%s, line %d) \n", strerror(errno),
				__func__,
				__FILE__, __LINE__);
		return -1;
	}

	this->fp = fp;

	return 0;
}

int getSampleCount(Sample* this) {
	FILE* fp = this->fp;
	char buf[256] = { 0, };
	int count = 0;

	while (fgets(buf, 256, fp) != NULL)
		count++;

	return count - 2;
}

int readLineSampleData(Sample* this, char* buffer, size_t size) {
	FILE* fp = this->fp;
	if (fgets(buffer, size, fp) == NULL)
		return -1;

	return strlen(buffer);
}

void disposeSample(Sample* this) {
	fclose(this->fp);
}
