
#ifndef __LIST_H__
#define __LIST_H__

#include <stdarg.h>
#include <sys/queue.h>

typedef struct _SLItem {
	void *vp;

	SLIST_ENTRY (_SLItem) datas;

	void (*set)(void*, void*);
	void* (*get)(void*);
} LItem;

LItem* newLItem();

void setLItem(LItem*, void*);

void* getLItem(LItem*);

typedef struct _SSList {
	SLIST_HEAD(slisthead, LItem) head;
	struct slisthead *headp;

	void (*addData)(void*, void*);
	LItem* (*getData)(void*, int);
	LItem* (*getFirstData)(void*);
	LItem* (*getNextData)(void*, void*);
	void (*dispose)(void*);
} SList;

SList* newList();

void addData(SList*, LItem*);

LItem* getData(SList*, int);

LItem* getFirstData(SList*);

LItem* getNextData(SList*, LItem*);

LItem* nextData(SList*);

void disposeList(SList*);

#endif
