/**
 * @file	bgentovcf.c
 * @date 	2018. 11. 4.
 * @author	D.M.SHIN
 * @brief	vcf
 *
 * @history	2019.04.26. - Add the '--gz' option
 *          2021.01.25. - Edit the help text
 *
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <time.h>

#include "vcfoption.h"
#include "vcfwriter.h"

void help();
void version();

int main(int argc, char *argv[]) {
	initVCFOptions(&vcfOptions);

	int option_index = 0;

	int opt;
	while (1) {
		static struct option options[] = {
				{ "gen-file", required_argument, 0, 'g' },
				{ "sample-file", required_argument, 0, 's' },
				{ "chr", required_argument, 0, 'c' },
				{ "digits", required_argument, 0, 'd' },
				{ "out", required_argument, 0, 'o' },
				{ "r_squared", no_argument, 0, 'R' },
				{ "progress", no_argument, 0, 'P' },
				{ "gz", no_argument, 0, 'G'},
				{ "help", no_argument, 0, 'h' },
				{ "version", no_argument, 0, 'v' },
				{ 0, 0, 0, 0 } };

		opt = getopt_long(argc, argv, "gsco:d:R:G:h:v:", options, &option_index);
		if (opt == -1)
			break;

		switch (opt) {
		case 'g':
			printf("option -%c with value '%s'\n", opt, optarg);
			setGenFilePath(optarg);
			break;
		case 's':
			printf("option -%c with value '%s'\n", opt, optarg);
			setSampleFilePath(optarg);
			break;
		case 'h':
//			printf("option -%c with value '%s'\n", opt, optarg);
			printf("option -h\n");
			help();
			exit(0);
		case 'o':
			printf("option -%c with value '%s'\n", opt, optarg);
			setOutputPath(optarg);
			break;
		case 'c':
			printf("option -%c with value '%s'\n", opt, optarg);
			setChrom(optarg);
			break;
		case 'd':
			printf("option -%c with value '%s'\n", opt, optarg);
			setDigits(atoi(optarg));
			break;
		case 'R':
			printf("option -R\n");
			setRsquaredEnable(true);
			break;
		case 'P':
			printf("option -P\n");
			setProgressEnable(true);
			break;
		case 'G':
			printf("option -G\n");
			setGZIPEnable(true);
			break;
		case 'v':
			printf("option -v\n");
			version();
			exit(0);
		case '?':
			printf("invalid option -%c\n", optopt);
			break;
		}
	}

	//	printf("%d %d\n", optind, argc);

	if (optind < argc) {
		printf("non-option ARGV-elements: ");
		while (optind < argc)
			printf("%s ", argv[optind++]);
		putchar('\n');
	}

	time_t start, end;
	start = clock();

	startConvert();

	destroyConvert();

	createtabix(vcfOptions.outputPath);

	end = clock();

	printf("Total time: %.2f ms\n", (double) (end - start));

	printf("%s\n", "Completed to convert GEN to VCF!");

	return 0;
}

void help() {
	printf("Usage: ./gen2vcf [OPTION] [FILE]\n"
			"  --help                          help\n"
			"  --gen-file [FILE]               genotype file\n"
			"  --sample-file [FILE]            sample file\n"
			"  --out [FILE]                    vcf file\n"
			"  --chr [CHROMOSOME]              chromosome\n"
			"  --r_squared                     r-squared\n"
			"  --gz                            gzipped genotype file\n"
			"  --digits [N]                    n digits after decimal point\n"
			"  --version                       version\n\n");
}

void version() {
	printf("Version 1.0.2\n\n");
}
