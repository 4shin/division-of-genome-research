/**
 * @file	numberutils.c
 * @date 	2017. 8. 26.
 * @author	D.M.SHIN
 * @brief	numberutils
 *
 * @history	2020.11.09. - Modify the doubleToString() function
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "numberutils.h"
//#include "ryu/ryu.h"

inline void removeFloatZeros(char* floatVal) {
	int len = strlen(floatVal);

	if (len == 1) {
		return;
	}

	char *p = floatVal + len - 1;
	while (*p == '0') {
		*p-- = 0x0;
	}

	if (*p == '.')
		*p-- = 0x0;
}

inline void doubleToString(double val, char* buf, int ndigit) {
//	d2fixed_buffered(f, 5, s);
	int dec, sign;
	char* str, *rst;

	rst = buf;

	str = fcvt(val, ndigit, &dec, &sign);

	if (dec <= 0) {
		if (dec < -ndigit)
			dec = -ndigit;
		*buf++ = '0';
		*buf++ = '.';

		while (dec++)
			*buf++ = '0';
	} else {
		while (dec--) {
			*buf++ = *str++;
			if (dec == 0)
				*buf++ = '.';
		}
	}

	strcpy(buf, str);
}
