#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

LItem* newLItem() {
	LItem* item = (LItem*) malloc(sizeof(LItem));

	item->vp = NULL;

	item->set = setLItem;
	item->get = getLItem;

	return item;
}

void setLItem(LItem* this, void* obj) {
	this->vp = obj;
}


void* getLItem(LItem* this) {
	return this->vp;
}

SList* newList() {
	SList* list = (SList*) malloc(sizeof(SList));

	SLIST_INIT(&list->head);

	list->addData = addData;
	list->getFirstData = getFirstData;
	list->getNextData = getNextData;
//	list->nextData = nextData;
	list->dispose = disposeList;

//	list->popPtr = NULL;

	return list;
}

void addData(SList* this, LItem* data) {
	static LItem* pushPtr;
//	LItem* new_data = newData();
//	new_data->setData(new_data, data);
	LItem* new_data = data;

	if (SLIST_EMPTY(&this->head)) {
		SLIST_INSERT_HEAD(&this->head, new_data, datas);
	} else {
		SLIST_INSERT_AFTER(pushPtr, new_data, datas);
	}

	pushPtr = new_data;
}

LItem* getFirstData(SList* this) {
	if (!SLIST_EMPTY(&this->head)) {
		return (LItem*) SLIST_FIRST(&this->head);
	}
	return NULL;
}

LItem* getNextData(SList* this, LItem* obj) {
	LItem* newObj;
	if (!SLIST_EMPTY(&this->head)) {
		if (obj->datas.sle_next == NULL)
			return NULL;
		newObj = SLIST_NEXT(obj, datas);
		return newObj;
	}
	return NULL;
}

void disposeList(SList* list) {
	LItem* ptr1, *ptr2;
	ptr1 = SLIST_FIRST(&list->head);
	while (ptr1 != NULL) {
		ptr2 = SLIST_NEXT(ptr1, datas);
//		if (ptr1->u.cp != NULL)
//			free(ptr1->u.cp);
		if(ptr1->vp != NULL)
			free(ptr1->vp);
		free(ptr1);
		ptr1 = ptr2;
	}

	SLIST_INIT(&list->head);

	free(list);
}
