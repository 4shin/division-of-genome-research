/**
 * @file	meta.h
 * @date 	2016. 11. 22.
 * @author	D.M.SHIN
 * @brief	meta information
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "meta.h"

const char* LINE_START_KEY_FILE_FORMAT = "##fileformat=";
const char* LINE_START_KEY_FILE_DATE = "##fileDate=";
const char* LINE_START_KEY_SOURCE = "##source=";
const char* LINE_START_KEY_REFERENCE = "##reference=";
const char* LINE_START_KEY_CONTIG = "##contig=";
const char* LINE_START_KEY_PHASING = "##phasing=";
const char* LINE_START_KEY_INFO = "##info=";
const char* LINE_START_KEY_FILTER = "##filter=";
const char* LINE_START_KEY_FORMAT = "##format=";

static const char* DELIMITER_NEWLINE = "\n";

void newVCFMetaContig() {
	VCFMetaContig* contig = calloc(1, sizeof(VCFMetaContig));

	contig->setId = setVCFMetaContigId;
	contig->setLength = setVCFMetaContigLength;
	contig->setAssembly = setVCFMetaContigAssembly;
	contig->dispose = disposeVCFMetaContig;
}

void setVCFMetaContigId(VCFMetaContig* this, char* id) {
	this->id = calloc(strlen(id) + 1, sizeof(char));
	strcpy(this->id, id);
}

void setVCFMetaContigLength(VCFMetaContig* this, long length) {
	this->length = length;
}

void setVCFMetaContigAssembly(VCFMetaContig* this, char* assembly) {
	this->assembly = calloc(strlen(assembly) + 1, sizeof(char));
	strcpy(this->assembly, assembly);
}

void disposeVCFMetaContig(VCFMetaContig* this) {
	if(this->id != NULL)
		free(this->id);
	if(this->assembly != NULL)
		free(this->assembly);
	free(this);
}

void newVCFMetaFilter() {
	VCFMetaFilter* filter = calloc(1, sizeof(VCFMetaFilter));

	filter->setId = setVCFMetaFilterId;
	filter->setDescription = setVCFMetaFilterDescription;
	filter->dispose = disposeVCFMetaFilter;
}

void setVCFMetaFilterId(VCFMetaFilter* this, char* id) {
	this->id = calloc(strlen(id) + 1, sizeof(char));
	strcpy(this->id, id);
}

void setVCFMetaFilterDescription(VCFMetaFilter* this, char* description) {
	this->description = calloc(strlen(description) + 1, sizeof(char));
	strcpy(this->description, description);
}

void disposeVCFMetaFilter(VCFMetaFilter* this) {
	if(this->id != NULL)
		free(this->id);
	if(this->description != NULL)
		free(this->description);
	free(this);
}

VCFMetaFormat* newVCFMetaFormat() {
	VCFMetaFormat* format = (VCFMetaFormat*) malloc(sizeof(VCFMetaFormat));

	format->id = NULL;
	format->number = NULL;
	format->type = NULL;
	format->description = NULL;

	format->setId = setVCFMetaFormatId;
	format->setNumber = setVCFMetaFormatNumber;
	format->setType = setVCFMetaFormatType;
	format->setDescription = setVCFMetaFormatDescription;
	format->getString = getVCFMetaFormatString;
	format->dispose = disposeVCFMetaFormat;

	return format;
}

void setVCFMetaFormatId(VCFMetaFormat* this, char* id) {
	char* newId = id;
	this->id = calloc(strlen(newId) + 1, sizeof(char));
	strcpy(this->id, newId);
}

void setVCFMetaFormatNumber(VCFMetaFormat* this, char* number) {
	char* newNumber = number;
	this->number = calloc(strlen(newNumber) + 1, sizeof(char));
	strcpy(this->number, newNumber);
}

void setVCFMetaFormatType(VCFMetaFormat* this, char* type) {
	char* newType = type;
	this->type = calloc(strlen(newType) + 1, sizeof(char));
	strcpy(this->type, newType);
}

void setVCFMetaFormatDescription(VCFMetaFormat* this, char* description) {
	char* newDescription = description;
	this->description = calloc(strlen(newDescription) + 1, sizeof(char));
	strcpy(this->description, newDescription);
}

void getVCFMetaFormatString(VCFMetaFormat* this, char* buf) {
	char* ptr = buf;
	strcpy(ptr, "##FORMAT=<");
	if(this->id != NULL) {
		strcat(ptr, "ID=");
		strcat(ptr, this->id);
	}
	if(this->number != NULL) {
		strcat(ptr, ",");
		strcat(ptr, "Number=");
		strcat(ptr, this->number);
	}
	if(this->type != NULL) {
		strcat(ptr, ",");
		strcat(ptr, "Type=");
		strcat(ptr, this->type);
	}
	if(this->description != NULL) {
		strcat(ptr, ",");
		strcat(ptr, "Description=\"");
		strcat(ptr, this->description);
		strcat(ptr, "\"");
	}
	strcat(ptr, ">");
}

void disposeVCFMetaFormat(VCFMetaFormat* this) {
	if(this->id != NULL)
		free(this->id);
	if(this->number != NULL)
		free(this->number);
	if(this->type != NULL)
		free(this->type);
	if(this->description != NULL)
		free(this->description);

	free(this);
}

VCFMetaInfo* newVCFMetaInfo() {
	VCFMetaInfo* info = (VCFMetaInfo*) malloc(sizeof(VCFMetaInfo));

	info->id = NULL;
	info->number = NULL;
	info->type = NULL;
	info->description = NULL;
	info->source = NULL;
	info->version = NULL;

	info->setId = setVCFMetaInfoId;
	info->setNumber = setVCFMetaInfoNumber;
	info->setType = setVCFMetaInfoType;
	info->setDescription = setVCFMetaInfoDescription;
	info->setSource = setVCFMetaInfoSource;
	info->setVersion = setVCFMetaInfoVersion;
	info->getString = getVCFMetaInfoString;
	info->dispose = disposeVCFMetaInfo;

	return info;
}

void setVCFMetaInfoId(VCFMetaInfo* this, char* id) {
	this->id = (char*) calloc(strlen(id) + 1, sizeof(char));
	strcpy(this->id, id);
}

void setVCFMetaInfoNumber(VCFMetaInfo* this, char* number) {
	this->number = (char*) calloc(strlen(number) + 1, sizeof(char));
	strcpy(this->number, number);
}

void setVCFMetaInfoType(VCFMetaInfo* this, char* type) {
	this->type = (char*) calloc(strlen(type) + 1, sizeof(char));
	strcpy(this->type, type);
}

void setVCFMetaInfoDescription(VCFMetaInfo* this, char* description) {
	this->description = (char*) calloc(strlen(description) + 1, sizeof(char));
	strcpy(this->description, description);
}

void setVCFMetaInfoSource(VCFMetaInfo* this, char* source) {
	this->source = (char*) calloc(strlen(source) + 1, sizeof(char));
	strcpy(this->source, source);
}

void setVCFMetaInfoVersion(VCFMetaInfo* this, char* version) {
	this->version = (char*) calloc(strlen(version) + 1, sizeof(char));
	strcpy(this->version, version);
}

void getVCFMetaInfoString(VCFMetaInfo* this, char* buffer) {
	char* ptr = buffer;
	strcpy(ptr, "##INFO=<");
	if(this->id != NULL) {
		strcat(ptr, "ID=");
		strcat(ptr, this->id);
	}
	if(this->number != NULL) {
		strcat(ptr, ",");
		strcat(ptr, "Number=");
		strcat(ptr, this->number);
	}
	if(this->type != NULL) {
		strcat(ptr, ",");
		strcat(ptr, "Type=");
		strcat(ptr, this->type);
	}
	if(this->description != NULL) {
		strcat(ptr, ",");
		strcat(ptr, "Description=\"");
		strcat(ptr, this->description);
		strcat(ptr, "\"");
	}
	strcat(ptr, ">");

//	printf("%s\n", ptr);
}

void disposeVCFMetaInfo(VCFMetaInfo* this) {
	if(this->id != NULL)
		free(this->id);
	if(this->number != NULL)
		free(this->number);
	if(this->type != NULL)
		free(this->type);
	if(this->description != NULL)
		free(this->description);
	if(this->source != NULL)
		free(this->source);
	if(this->version != NULL)
		free(this->version);

	free(this);
}

VCFMeta* newVCFMeta() {
	VCFMeta* meta = (VCFMeta*) malloc(sizeof(VCFMeta));

	meta->fileformat = NULL;
	meta->fileDate = NULL;
	meta->source = NULL;
	meta->contigList = NULL;
	meta->source = NULL;
	meta->reference = NULL;
	meta->phasing = NULL;
	meta->infoList = NULL;
	meta->formatList = NULL;
	meta->filterList = NULL;

	meta->init = initVCFMeta;
	meta->setFileFormat = setVCFFileFormat;
	meta->setFileDate = setVCFFileDate;
	meta->setSource = setVCFSource;
	meta->setContig = setVCFContig;
	meta->setReference = setVCFReference;
	meta->setPhasing = setVCFPhasing;
	meta->setInfo = setVCFInfo;
	meta->setFormat = setVCFFormat;
	meta->setFilter = setVCFFilter;
	meta->getText = getVCFMetaText;
	meta->dispose = disposeVCFMeta;

	return meta;
}

void setVCFFileFormat(VCFMeta* this, const char* fileFormat) {
//	fprintf(stdout, "%s\n", "setVCFFileFormat");

	this->fileformat = (char*) malloc(sizeof(char) * (strlen(fileFormat) + 1));
	strcpy(this->fileformat, fileFormat);
}

void setVCFFileDate(VCFMeta* this, const char* fileDate) {
//	fprintf(stdout, "%s\n", "setVCFFileDate");

	this->fileDate = (char*) malloc(sizeof(char) * (strlen(fileDate) + 1));
	strcpy(this->fileDate, fileDate);
}

void setVCFSource(VCFMeta* this, char* source) {
//	fprintf(stdout, "%s\n", "setVCFSource");

	this->source = (char*) malloc(sizeof(char) * (strlen(source) + 1));
	strcpy(this->source, source);
}

void setVCFContig(VCFMeta* this, SList* list) {
	this->contigList = list;
}

void setVCFReference(VCFMeta* this, char* reference) {
	this->reference = (char*) malloc(sizeof(char) * (strlen(reference) + 1));
	strcpy(this->reference, reference);
}

void setVCFPhasing(VCFMeta* this, char* phasing) {
	this->phasing = (char*) malloc(sizeof(char) * (strlen(phasing) + 1));
	strcpy(this->phasing, phasing);
}

void setVCFInfo(VCFMeta* this, SList* list) {
	this->infoList = list;
}

void setVCFFormat(VCFMeta* this, SList* list) {
	this->formatList = list;
}

void setVCFFilter(VCFMeta* this, SList* list) {
	this->filterList = list;
}

int getVCFMetaText(VCFMeta* this, char* text) {
	strcpy(text, LINE_START_KEY_FILE_FORMAT);
	strcat(text, this->fileformat);
	strcat(text, DELIMITER_NEWLINE);
	strcat(text, LINE_START_KEY_FILE_DATE);
	strcat(text, this->fileDate);
	strcat(text, DELIMITER_NEWLINE);

	char buf[1024];

 	LItem* item;
 	VCFMetaInfo* info;

 	if(this->infoList != NULL) {
		if((item = this->infoList->getFirstData(this->infoList)) != NULL) {
	 		info = item->get(item);
	 		info->getString(info, buf);
	 		strcat(text, buf);
	 		strcat(text, DELIMITER_NEWLINE);
		}

		while ((item = this->infoList->getNextData(this, item)) != NULL) {
				info = item->get(item);
				info->getString(info, buf);

				strcat(text, buf);
				strcat(text, DELIMITER_NEWLINE);
		}
 	}

 	VCFMetaFormat* format;
 	if(this->formatList != NULL) {
		if((item = this->formatList->getFirstData(this->formatList)) != NULL) {
			format = item->get(item);
			format->getString(format, buf);
			strcat(text, buf);
			strcat(text, DELIMITER_NEWLINE);
		}

		while ((item = this->formatList->getNextData(this, item)) != NULL) {
			format = item->get(item);
			format->getString(format, buf);
			strcat(text, buf);
			strcat(text, DELIMITER_NEWLINE);
		}
 	}

	return strlen(text);
}

void disposeVCFMeta(VCFMeta* this) {
	if (this->fileformat != NULL)
		free(this->fileformat);
	if (this->fileDate != NULL)
		free(this->fileDate);
	if (this->source != NULL)
		free(this->source);
	if (this->contigList != NULL)
		this->contigList->dispose(this->contigList);
	if (this->reference != NULL)
		free(this->reference);
	if (this->phasing != NULL)
		free(this->phasing);
	if (this->infoList != NULL)
		this->infoList->dispose(this->infoList);
	if (this->filterList != NULL)
		this->filterList->dispose(this->filterList);
	if (this->formatList != NULL)
		this->formatList->dispose(this->formatList);
}

void initVCFMeta(VCFMeta* meta) {
	meta->setFileFormat(meta, VCF_VERSION);

	time_t timer;
	time(&timer);
	struct tm *t = localtime(&timer);
	char buf[255];
	memset(buf, 0x00, sizeof(buf));
//	sprintf(buf, "%04d %02d %02d", t->tm_year, t->tm_mon + 1, t->tm_mday);
//	sprintf(buf, "%04d%02d%02d", t->tm_year + 1900, t->tm_mon, t->tm_mday);
	strftime(buf, sizeof(buf), "%Y%m%d", t);
	meta->setFileDate(meta, buf);

	// INFO
	SList* infoList = NULL;
	LItem* item;

	if(vcfOptions.r2Yn) {
		infoList = newList();

		infoList = newList();
		item = newLItem();
		VCFMetaInfo* r2 = newVCFMetaInfo();
		r2->setId(r2, "R2");
		r2->setNumber(r2, "1");
		r2->setType(r2, "Float");
		r2->setDescription(r2, "Estimated r-squared (r2) from the imputed dosages");
		item->set(item, r2);
		infoList->addData(infoList, item);

		meta->infoList = infoList;
	}

	// FORMAT
	SList* formatList = newList();
	item = newLItem();
	VCFMetaFormat* gt = newVCFMetaFormat();
	gt->setId(gt, "GT");
	gt->setNumber(gt, "1");
	gt->setType(gt, "String");
	gt->setDescription(gt, "Genotype");
	item->set(item, gt);
	formatList->addData(formatList, item);

	item = newLItem();
	VCFMetaFormat* ds = newVCFMetaFormat();
	ds->setId(ds, "DS");
	ds->setNumber(ds, "1");
	ds->setType(ds, "Float");
	ds->setDescription(ds, "Genotype Dosage");
	item->set(item, ds);
	formatList->addData(formatList, item);

	item = newLItem();
	VCFMetaFormat* gp = newVCFMetaFormat();
	gp->setId(gp, "GP");
	gp->setNumber(gp, "G");
	gp->setType(gp, "Float");
	gp->setDescription(gp, "Genotype Probability");
	item->set(item, gp);
	formatList->addData(formatList, item);

	meta->formatList = formatList;
}
