/**
 * @file	sample.h
 * @date 	2016. 10. 16.
 * @author	D.M.SHIN
 * @brief	sample
 *
 * @history	2017.06.03. - Changed a structure name from 'KChip' to 'Sample'.
 */

#include <stdio.h>
#include <sys/queue.h>

/**
 * @brief	sample structure
 */
typedef struct _SSample {
	FILE* fp;

	int (*open)(void*, const char*);
	int (*readLine)(void*, char*, size_t);
	int (*readDataFrom)(void*, char*);
	int (*getCount)(void*);
	void (*dispose)(void*);
} Sample;

/**
 * @brief	create a new instance.
 * @return	new instance.
 */
Sample* newSample();

/**
 * @brief	open sample file.
 * @param	file path.
 * @return	File pointer for success, NULL for failed.
 */
int openSampleFile(Sample*, const char*);

/**
 * @brief	read a line from sample file.
 * @param	file path.
 * @return	File pointer for success, NULL for failed.
 */
int readLineSampleData(Sample*, char*, size_t size);

/**
 * @brief	get number of samples.
 * @return	number of samples.
 */
int getSampleCount(Sample*);

/**
 * @brief	destroy the instance.
 */
void disposeSample(Sample*);
