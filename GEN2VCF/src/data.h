/**
 * @file	data.h
 * @date 	2017. 9. 6.
 * @author	D.M.SHIN
 * @brief	VCF data
 */

#ifndef __DATA_H__
#define __DATA_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

/**
 * @brief	genotypes data structure
 */
typedef struct _SVCFSampleGData {
	char gp[128];
	double ds;
	char gt[32];
} VCFSampleGData;

/**
 * @brief	genotypes data structure
 */
typedef struct _SVCFInfo {
	double r2;
	char* is;

	void (*setR2)(void*, double);
	double (*getR2)(void*);

	void (*setIS)(void*, char*);
	char* (*getIS)(void*);

	int (*getString)(void*, char*);

	void (*dispose)(void*);

} VCFSNPInfo;

VCFSNPInfo* newVCFSNPInfo();

void setR2(VCFSNPInfo*, double);
double getR2(VCFSNPInfo*);

void setIS(VCFSNPInfo*, char*);
char* getIS(VCFSNPInfo*);

int getVCFInfoString(VCFSNPInfo*, char*);

void disposeVCFInfo(VCFSNPInfo*);

typedef struct _SVCFVarRecord {
	char* chrom;
	char* pos;
	char* id;
	char* ref;
	char* alt;
	char* qual;
	char* filter;
	void* info;
	char* format;
	SList* samples;

	unsigned int infoCount;
	unsigned int sampleCount;

	void (*setChrom)(void*, void*);
	char* (*getChrom)(void*);
	void (*setID)(void*, void*);
	char* (*getID)(void*);
	void (*setPos)(void*, void*);
	char* (*getPos)(void*);
	void (*setRef)(void*, void*);
	char* (*getRef)(void*);
	void (*setAlt)(void*, void*);
	char* (*getAlt)(void*);
	void (*setQual)(void*, void*);
	char* (*getQual)(void*);
	void (*setFilter)(void*, void*);
	char* (*getFilter)(void*);
	void (*setInfo)(void*, void*);
	void (*setFormat)(void*, void*);
	char* (*getFormat)(void*);
	void (*addSample)(void*, void*);
	void (*addSamples)(void*, void**, size_t);
	void (*samplesToString)(void*, char*);
	char* (*getString)(void*);
	void (*dispose)(void*);
} VCFVarRecord;

VCFVarRecord* NewVCFVarRecord();

void setVarChrom(VCFVarRecord*, char*);
char* geVartChrom(VCFVarRecord*);

void setVarID(VCFVarRecord*, char*);
char* getVarID(VCFVarRecord*);

void setVarPos(VCFVarRecord*, char*);
char* getVarPos(VCFVarRecord*);

void setVarRef(VCFVarRecord*, char*);
char* getVarRef(VCFVarRecord*);

void setVarAlt(VCFVarRecord*, char*);
char* getVarAlt(VCFVarRecord*);

void setVarFilter(VCFVarRecord*, char*);
char* getVarFilter(VCFVarRecord*);

void setVarInfo(VCFVarRecord*, VCFSNPInfo*);

void setVarFormat(VCFVarRecord*, char*);
char* getVarFormat(VCFVarRecord*);

void addVarSample(VCFVarRecord*, VCFSampleGData*);
void addVarSamples(VCFVarRecord*, VCFSampleGData**, size_t);

void varSamplesToString(VCFVarRecord*, char*);

char* getVarToString(VCFVarRecord*);

void disposeVCFData(VCFVarRecord*);

#endif
