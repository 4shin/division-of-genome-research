/**
 * @file	data.h
 * @date 	2017. 9. 6.
 * @author	D.M.SHIN
 * @brief	VCF data
 *
 * @history	2018.2.9.  - Changed gcvt() into getDecimal()
 *          2018.9.18. - Changed the if statement's condition to 'this->is==NULL' from 'this->is!=NULL' in getVCFInfoString function
 *
 */

#include <stdlib.h>
#include <math.h>

#include "data.h"
#include "numberutils.h"
#include "vcfoption.h"
#include "list.h"

#define MAX3(a, b, c) ((a)>(b) ? ((a)>(c) ? (a) : (c)) : ((b)>(c) ? (b) : (c)))

VCFSNPInfo* newVCFSNPInfo() {
	VCFSNPInfo* info = (VCFSNPInfo*) malloc(sizeof(VCFSNPInfo));
	memset(info, 0x00, sizeof(VCFSNPInfo));

	info->r2 = NAN;

	info->setR2 = setR2;
	info->getR2 = getR2;
	info->getString = getVCFInfoString;
	info->dispose = disposeVCFInfo;

	return info;
}

void setR2(VCFSNPInfo* this, double val) {
	this->r2 = val;
}

double getR2(VCFSNPInfo* this) {
	return this->r2;
}

int getVCFInfoString(VCFSNPInfo* this, char* buf) {
	char* ptr = buf;
	int len;

	if (isnan(this->r2)) {
		len = strlen(".");
		memcpy(ptr, ".", len);
		ptr += len;
	} else {
		len = strlen("R2=");
		memcpy(ptr, "R2=", len);
		ptr += len;

		char r2OfString[64];

		if (this->r2 < 0) {
			len = strlen("NA");
			memcpy(r2OfString, "NA", len);
		} else {
			doubleToString(this->r2, r2OfString, vcfOptions.digits);
			removeFloatZeros(r2OfString);
		}

		len = strlen(r2OfString);
		memcpy(ptr, r2OfString, len);
		ptr += len;
	}

	*ptr = 0x0;

	return (int) (ptr - buf);
}

void disposeVCFInfo(VCFSNPInfo* this) {
	free(this->is);
	free(this);
}


void setVarChrom(VCFVarRecord* this, char* data) {
	this->chrom = strdup(data);
}

char* getVarChrom(VCFVarRecord* this) {
	return this->chrom;
}

void setVarID(VCFVarRecord* this, char* data) {
	this->id = strdup(data);
}

char* getVarID(VCFVarRecord* this) {
	return this->id;
}

void setVarPos(VCFVarRecord* this, char* data) {
	this->pos = strdup(data);
}

char* getVarPos(VCFVarRecord* this) {
	return this->pos;
}

void setVarRef(VCFVarRecord* this, char* data) {
	this->ref = strdup(data);
}

char* getVarRef(VCFVarRecord* this) {
	return this->ref;
}

void setVarAlt(VCFVarRecord* this, char* data) {
	this->alt = strdup(data);
}

char* getVarAlt(VCFVarRecord* this) {
	return this->alt;
}

void setVarQual(VCFVarRecord* this, char* data) {
	this->qual = strdup(data);
}

char* getVarQual(VCFVarRecord* this) {
	return this->qual;
}

void setVarFilter(VCFVarRecord* this, char* data) {
	this->filter = strdup(data);
}

char* getVarFilter(VCFVarRecord* this) {
	return this->filter;
}

void setVarInfo(VCFVarRecord* this, VCFSNPInfo* info) {
	this->info = info;
}

void setVarFormat(VCFVarRecord* this, char* data) {
	this->format = strdup(data);
}

char* getVarFormat(VCFVarRecord* this) {
	return this->format;
}

void setVarSamples(VCFVarRecord* this, char** data) {
//	this->samples = data;
//	this->sampleCount = count;
}

void addVarSample(VCFVarRecord* this, VCFSampleGData* data) {
	if(this->samples == NULL) {
		this->samples = newList();
	}

	char buf[512];
	char* ptr = buf;
	int len;

	// GT
	char* gt = data->gt;
	len = strlen(gt);
	memcpy(ptr, gt, len);
	ptr += len;

	memcpy(ptr, ":", 1);
	ptr += 1;

	// DS
	double n_ds = data->ds;
	char s_ds[32];
	doubleToString(n_ds, s_ds, vcfOptions.digits);
	removeFloatZeros(s_ds);
	len = strlen(s_ds);
	memcpy(ptr, s_ds, len);
	ptr += len;

	memcpy(ptr, ":", 1);
	ptr += 1;

	// GP
	char* gp = data->gp;
	len = strlen(gp);
	memcpy(ptr, gp, len);
	ptr += len;

	*ptr = 0x0;

	LItem* item = newLItem();
	item->set(item, strdup(buf));
	this->samples->addData(this->samples, item);
}

void addVarSamples(VCFVarRecord* this, VCFSampleGData** data, size_t n) {
	if(this->samples == NULL) {
		this->samples = newList();
	}

	int i;
	char buf[512];
	char* ptr = buf;
	int len;
	for (i = 0; i < n; i++) {
		// GT
		char* gt = data[i]->gt;
		len = strlen(gt);
		memcpy(ptr, gt, len);
		ptr += len;

		memcpy(ptr, ":", 1);
		ptr += 1;

		// DS
		double n_ds = data[i]->ds;
		char s_ds[32];
		doubleToString(n_ds, s_ds, vcfOptions.digits);
		removeFloatZeros(s_ds);
		len = strlen(s_ds);
		memcpy(ptr, s_ds, len);
		ptr += len;

		memcpy(ptr, ":", 1);
		ptr += 1;

		// GP
		char* gp = data[i]->gp;
		len = strlen(gp);
		memcpy(ptr, gp, len);
		ptr += len;

		*ptr = 0x0;
		ptr = buf;

		LItem* item = newLItem();
		item->set(item, strdup(buf));
		this->samples->addData(this->samples, item);
	}
}

void varSamplesToString(VCFVarRecord* this, char* buf) {
	SList* samples = this->samples;
	LItem* item;
	char* ptr = buf;
	int len;

	if ((item = samples->getFirstData(samples)) != NULL) {
//		printf("%s\n", (char*) item->get(item));
		len = strlen((char*) item->get(item));
		memcpy(ptr, (char*) item->get(item), len);
		ptr += len;

		memcpy(ptr, "\t", 1);
		ptr += 1;
	}

	while ((item = samples->getNextData(samples, item)) != NULL) {
//		printf("%s\n", (char*) item->get(item));
		len = strlen((char*) item->get(item));
		memcpy(ptr, (char*) item->get(item), len);
		ptr += len;

		memcpy(ptr, "\t", 1);
		ptr += 1;
	}

	*ptr = 0x0;
}

char* getVarToString(VCFVarRecord* this) {
	int len = 0;
	len += strlen(this->getChrom(this));
	len += strlen(this->getPos(this));
	len += strlen(this->getChrom(this));
	len += strlen(this->getPos(this));
	len += strlen(this->getRef(this));
	len += strlen(this->getAlt(this));
	len += strlen(this->getRef(this));
	len += strlen(this->getAlt(this));
	len += strlen(this->getQual(this));
	len += strlen(this->getFilter(this));
	len += 10;

	char info[256];
	((VCFSNPInfo*) this->info)->getString((VCFSNPInfo*) this->info, info);
	len += strlen(info);
	len += 1;
	len += strlen(this->getFormat(this));

	SList* samples = this->samples;
	LItem* item;
	if ((item = samples->getFirstData(samples)) != NULL) {
		len += 1;
		len += strlen(item->get(item));
	}
	while ((item = samples->getNextData(samples, item)) != NULL) {
		len += 1;
		len += strlen(item->get(item));
	}

	int size = sizeof(char) * len + 1;
	char* line = malloc(size);
	memset(line, 0x00, size);

	strcpy(line, this->getChrom(this));
	strcat(line, "\t");
	strcat(line, this->getPos(this));
	strcat(line, "\t");
	strcat(line, this->getChrom(this));
	strcat(line, ":");
	strcat(line, this->getPos(this));
	strcat(line, "_");
	strcat(line, this->getRef(this));
	strcat(line, "/");
	strcat(line, this->getAlt(this));
	strcat(line, "\t");
	strcat(line, this->getRef(this));
	strcat(line, "\t");
	strcat(line, this->getAlt(this));
	strcat(line, "\t");
	strcat(line, this->getQual(this));
	strcat(line, "\t");
	strcat(line, this->getFilter(this));
	strcat(line, "\t");
	strcat(line, info);
	strcat(line, "\t");
	strcat(line, this->getFormat(this));


	if ((item = samples->getFirstData(samples)) != NULL) {
//		printf("%s\n", (char*) item->get(item));
		strcat(line, "\t");
		strcat(line, (char*) item->get(item));
	}

	while ((item = samples->getNextData(samples, item)) != NULL) {
//		printf("%s\n", (char*) item->get(item));
		strcat(line, "\t");
		strcat(line, (char*) item->get(item));
	}

	return line;
}

void disposeVCFData(VCFVarRecord* this) {
	if (this->chrom != NULL)
		free(this->chrom);
	if (this->pos != NULL)
		free(this->pos);
	if (this->id != NULL)
		free(this->id);
	if (this->ref != NULL)
		free(this->ref);
	if (this->alt != NULL)
		free(this->alt);
	if (this->qual != NULL)
		free(this->qual);
	if (this->filter != NULL)
		free(this->filter);
	if (this->info != NULL)
		((VCFSNPInfo*) this->info)->dispose(((VCFSNPInfo*) this->info));
	if (this->format != NULL)
		free(this->format);

	SList* samples = this->samples;
	if (samples != NULL) {
		samples->dispose(samples);
//		free(samples);
	}

	free(this);
}

VCFVarRecord* NewVCFVarRecord() {
	VCFVarRecord* snpData = malloc(sizeof(VCFVarRecord));

	snpData->chrom = NULL;
	snpData->pos = NULL;
	snpData->id = NULL;
	snpData->ref = NULL;
	snpData->alt = NULL;
	snpData->qual = NULL;
	snpData->filter = NULL;
	snpData->info = NULL;
	snpData->format = NULL;
//	snpData->samples = NULL;
	snpData->samples = NULL;

	snpData->setChrom = setVarChrom;
	snpData->getChrom = getVarChrom;
	snpData->setID = setVarID;
	snpData->getID = getVarID;
	snpData->setPos = setVarPos;
	snpData->getPos = getVarPos;
	snpData->setRef = setVarRef;
	snpData->getRef = getVarRef;
	snpData->setAlt = setVarAlt;
	snpData->getAlt = getVarAlt;
	snpData->setQual = setVarQual;
	snpData->getQual = getVarQual;
	snpData->setFilter = setVarFilter;
	snpData->getFilter = getVarFilter;
	snpData->setInfo = setVarInfo;
	snpData->setFormat = setVarFormat;
	snpData->getFormat = getVarFormat;
	snpData->addSample = addVarSample;
	snpData->addSamples = addVarSamples;
	snpData->samplesToString = varSamplesToString;

	snpData->getString = getVarToString;
	snpData->dispose = disposeVCFData;

	return snpData;
}
