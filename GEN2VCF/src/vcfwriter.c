/**
 * @file	vcf.c
 * @date 	2018. 11. 5.
 * @author	D.M.SHIN
 * @brief	vcf
 *
 * @history	2018.12.04. - Define the createtabix function
 *          2019.04.26. - Define the getGZIPEnable function
 */

#include "vcfwriter.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <zlib.h>

#include "sample.h"
#include "header.h"
#include "data.h"
#include "list.h"
#include "vcfoption.h"
#include "tabix.h"
#include "numberutils.h"

#define MAX3(a, b, c) ((a)>(b) ? ((a)>(c) ? (a) : (c)) : ((b)>(c) ? (b) : (c)))

int sampleCount = 0;

extern inline void calcGT(double aa, double ab, double bb, char* buf) {
	double max = MAX3(aa, ab, bb);
	if (max == aa) {
		strcpy(buf, "0/0");
	} else if (max == ab) {
		strcpy(buf, "0/1");
	} else {
		strcpy(buf, "1/1");
	}
}

extern inline double calcDS(double ab, double bb) {
	double rst;
	rst = ab + bb * 2.0;

	return rst;
}

extern inline void calcGP(double aa, double ab, double bb, char* buf) {
	char temp[64];
	doubleToString(aa, temp, vcfOptions.digits);
	removeFloatZeros(temp);
	strcpy(buf, temp);
	strcat(buf, ",");
	doubleToString(ab, temp, vcfOptions.digits);
	removeFloatZeros(temp);
	strcat(buf, temp);
	strcat(buf, ",");
	doubleToString(bb, temp, vcfOptions.digits);
	removeFloatZeros(temp);
	strcat(buf, temp);
}


void convertToVCFFormat(char* list[], int size) {
//	printf("%s\n", buf);

	VCFVarRecord* data = NULL;
	data = NewVCFVarRecord();
	data->setChrom(data, vcfOptions.chr);
	data->setPos(data, list[2]);
	data->setRef(data, list[3]);
	data->setAlt(data, list[4]);
	data->setQual(data, ".");
	data->setFilter(data, ".");
	data->setFormat(data, "GT:DS:GP");
	VCFSNPInfo* snpInfo = newVCFSNPInfo();
	data->setInfo(data, snpInfo);

	double meanAB = 0.f;
	double meanBB = 0.f;
	double sumX2 = 0.f;
	double sumXbar2 = 0.f;
	double meanX = 0.f;
	double rSqHat = 0.f;

	size_t i;
	size_t idx = 0;
	for (i = 0; i < sampleCount ; i++) {
//			if (list[i] == NULL) break;
//			printf("%d - %s\n", omp_get_thread_num(), list[i]);
		idx = i * 3;
		double aa = atof(list[idx + 5]);
		double ab = atof(list[idx + 6]);
		double bb = atof(list[idx + 7]);

		char buf[128];

		VCFSampleGData sampleGData;
		calcGP(aa, ab, bb, buf);
		strcpy(sampleGData.gp, buf);
		double ds = calcDS(ab, bb);
		sampleGData.ds = ds;
		calcGT(aa, ab, bb, buf);
		strcpy(sampleGData.gt, buf);

		data->addSample(data, &sampleGData);

		if (vcfOptions.r2Yn) {
			meanAB += ab;
			meanBB += bb;

			sumX2 += ab + 4.0 * bb;
			sumXbar2 += pow(ds, 2.0);
		}
	}

	if (vcfOptions.r2Yn) {
		if (!meanAB && !meanBB) {
			rSqHat = -1;
		} else {
			meanX = (meanAB + 2.0 * meanBB) / (double) sampleCount;
			rSqHat = ((sumXbar2 / (double) sampleCount) - pow(meanX, 2.0))
					/ ((sumX2 / (double) sampleCount) - pow(meanX, 2.0));
		}

		snpInfo->setR2(snpInfo, rSqHat);
	}

	char* line;
	line = data->getString(data);
	bgzf_write(vcffp, line, strlen(line));
	bgzf_write(vcffp, "\n", 1);

	free(line);

	data->dispose(data);
}

void initConvert() {
	vcffp = NULL;
	vcfMeta = NULL;
	vcfHeader = NULL;
}

void startConvert() {
	initConvert();

	if ((vcffp = bgzf_open(vcfOptions.outputPath, "w")) == NULL) {
		fprintf(stderr, "\n[ERROR] %s: %s (%s, line %d) \n", strerror(errno),
				__func__,
				__FILE__, __LINE__);
		bgzf_close(vcffp);
		exit(1);
	}

	vcfMeta = newVCFMeta();
	vcfMeta->init(vcfMeta);
	if (writeVCFMeta(vcfMeta) < 0) {
		bgzf_close(vcffp);
		exit(1);
	}

	vcfHeader = newVCFHeader();
	vcfHeader->init(vcfHeader);
	if (writeVCFHeader(vcfHeader) < 0) {
		bgzf_close(vcffp);
		exit(1);
	}

	writeVCFData();
}

/*
 * @brief	write vcf meta.
 */
size_t writeVCFMeta(VCFMeta* meta) {
	printf("%s\n", "Writing meta-information in VCF file..");

	char buf[BUFSIZ] = { 0, };
	int len = meta->getText(meta, buf);

//	printf("%s\n", buf);
	bgzf_write(vcffp, buf, len);

	return strlen(buf);
}

/*
 * @brief	write vcf header.
 */
size_t writeVCFHeader(VCFHeader* header) {
	printf("%s\n", "Writing header in VCF file..");

	size_t len = header->getSize(header);
	char buf[len + 1];
	memset(buf, 0x00, len + 1);

	header->getText(header, buf);

	bgzf_write(vcffp, buf, strlen(buf));

//	bgzf_close(vcffp);

	return strlen(buf);
}

void writeVCFData() {
	printf("%s\n", "Writing data in VCF file..");

	gzFile fp_gz = NULL;
	FILE *fp = NULL;

	if (getGZIPEnable()) {
		if ((fp_gz = gzopen(vcfOptions.genFilePath, "r")) == NULL) {
			fprintf(stderr, "\n[ERROR] %s: %s (%s, line %d) \n",
					strerror(errno), __func__, __FILE__, __LINE__);
			exit(1);
		}
	} else if (!getGZIPEnable()) {
		if ((fp = fopen(vcfOptions.genFilePath, "r")) == NULL) {
			fprintf(stderr, "\n[ERROR] %s: %s (%s, line %d) \n",
					strerror(errno), __func__, __FILE__, __LINE__);
	//		return -1;
			exit(1);
			}
	} else {
		fp = stdin;
	}

	char buf1[1024] = { 0, };
	char* ptr1 = buf1;

	Sample* sample = newSample();
	if (sample->open(sample, vcfOptions.sampleFilePath) == -1) {
		exit(1);
	}
//	int sNum = sample->getCount(sample);
	sampleCount = sample->getCount(sample);
	sample->dispose(sample);
//	printf("%d\n", sNum);

//	int buf2size = sNum * 3 + 128;
	int buf2size = sampleCount * 3 + 128;
	char* buf2[buf2size];
	int idx2;
	for (idx2 = 0; idx2 < buf2size; idx2++) {
		buf2[idx2] = NULL;
	}

	idx2 = 0;
	int ch;
//	while ((ch = fgetc(genfp)) != EOF) {
	while (1) {
		if (getGZIPEnable()) {
			if ((ch = gzgetc(fp_gz)) == EOF) break;
		} else {
			if ((ch = fgetc(fp)) == EOF) break;
		}

//		printf("%c", ch);
		if (ch == '\n') {
			buf2[idx2++] = strdup(buf1);
			memset(buf1, 0x00, sizeof(buf1));
			ptr1 = buf1;

			convertToVCFFormat(buf2, buf2size);

			int i = 0;
			for (i = 0; i < buf2size; i++) {
				free(buf2[i]);
				buf2[i] = NULL;
			}

			idx2 = 0;
//			break;
		} else if (isspace(ch)) {
			buf2[idx2++] = strdup(buf1);
//			printf("%s ", buf1);
			memset(buf1, 0x00, sizeof(buf1));
			ptr1 = buf1;
		} else {
			memcpy(ptr1, &ch, 1);
			*ptr1++ = ch;
		}
	}

	if (fp != NULL) fclose(fp);
	if (fp_gz != NULL) gzclose(fp_gz);
}

void destroyConvert() {
	if (vcfMeta != NULL) {
		vcfMeta->dispose(vcfMeta);
	}

	if (vcfHeader != NULL) {
		vcfHeader->dispose(vcfHeader);
	}

	if(vcffp != NULL) {
		bgzf_close(vcffp);
	}
}

void createtabix(const char* infile) {
	tabix_t *t;

	if ((t = ti_open(infile, 0)) == 0) {
		fprintf(stderr, "[tabix] failed to open the data file.\n");
		exit(1);
	}

	ti_iter_t iter;
	ti_conf_t conf = ti_conf_gff, *conf_ptr = NULL;
	conf_ptr = &ti_conf_vcf;
	conf = *conf_ptr;

	iter = ti_query(t, 0, 0, 0);

	if (ti_index_build(infile, &conf) < 0) {
		fprintf(stderr, "[tabix] failed to open the data file.\n");
		exit(1);
	}

	ti_iter_destroy(iter);
	ti_close(t);
}
