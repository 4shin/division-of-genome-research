/**
 * @file	vcfoption.c
 * @date 	2017. 12. 12.
 * @author	D.M.SHIN
 * @brief	Tool Option
 *
 */

#include <string.h>

#include "vcfoption.h"

void initVCFOptions(VCFOptions* this) {
	this->genFilePath = NULL;
	this->infoFilePath = NULL;
	this->sampleFilePath = NULL;
	this->outputPath = NULL;
	this->chr = NULL;
	this->digits = 3;
	this->r2Yn = false;
	this->progressYn = false;
	this->gzipYN = false;
}

void setGenFilePath(char* arg) {
	vcfOptions.genFilePath = strdup(arg);
}
char* getGenFilePath() {
	return vcfOptions.genFilePath;
}

void setSampleFilePath(char* arg) {
	vcfOptions.sampleFilePath = strdup(arg);
}
char* getSampleFilePath() {
	return vcfOptions.sampleFilePath;
}

void setOutputPath(char* arg) {
	vcfOptions.outputPath = strdup(arg);
}
char* getOutputPath() {
	return vcfOptions.outputPath;
}

void setChrom(char* arg) {
	vcfOptions.chr = strdup(arg);
}
char* getChrom() {
	return vcfOptions.chr;
}

void setDigits(int arg) {
	vcfOptions.digits = arg;
}
int getDigits() {
	return vcfOptions.digits;
}

void setRsquaredEnable(boolean b) {
	vcfOptions.r2Yn = b;
}
boolean getRsquaredEnable() {
	return vcfOptions.r2Yn;
}

void setProgressEnable(boolean b) {
	vcfOptions.progressYn = b;
}
boolean getProgressEnable() {
	return vcfOptions.progressYn;
}

void setGZIPEnable(boolean b) {
	vcfOptions.gzipYN = b;
}

boolean getGZIPEnable() {
	return vcfOptions.gzipYN;
}
